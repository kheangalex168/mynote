//
//  Note.swift
//  Notes
//
//  Created by Kheang on 5/11/21.
//

import Foundation

class Note {
    let id = UUID()
    var text: String = ""
    var lastUpdate: Date = Date()

    var title: String {
        return text.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: .newlines).first ?? "" // returns the first line of the text
    }

    var desc: String {
        var lines = text.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: .newlines)
        lines.removeFirst()
        return "\(lines.first ?? "")" // return second line
    }
    
    var date: String {
        return lastUpdate.format()
    }
}

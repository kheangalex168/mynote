//
//  Note+CoreDataProperties.swift
//  Notes
//
//  Created by Kheang on 4/11/21.
//
//

import Foundation
import CoreData


extension Notes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Notes> {
        return NSFetchRequest<Notes>(entityName: "Notes")
    }

    @NSManaged public var desc: String!
    @NSManaged public var id: UUID!
    @NSManaged public var text: String!
    @NSManaged public var lastUpdate: Date!
    @NSManaged public var title: String!

}

extension Notes : Identifiable {

}

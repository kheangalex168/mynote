//
//  CoreDataManager.swift
//  Notes
//
//  Created by Kheang on 5/11/21.
//

import Foundation
import CoreData

class CoreDataManager{
     

    static let shared = CoreDataManager(modelName: "Notes")
    
    let persistentContainer: NSPersistentContainer
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    init(modelName: String){
      persistentContainer = NSPersistentContainer(name: modelName)
    }
    
    func load(completion: (() -> Void)? = nil){
        persistentContainer.loadPersistentStores{
            (description, error) in
            guard error == nil else {
                fatalError(error!.localizedDescription)
            }
            completion?()
        }
    }
    
    func save(){
        if viewContext.hasChanges {
            do{
                try viewContext.save()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

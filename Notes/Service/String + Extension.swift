//
//  String + Extension.swift
//  Notes
//
//  Created by Kheang on 5/11/21.
//

import Foundation
import UIKit

extension String {
    func localizeSystem() -> String {
            return NSLocalizedString(self, comment: self)
    }
    
    func localizedString() -> String {
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")
        
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: self, comment: self)
    }
}

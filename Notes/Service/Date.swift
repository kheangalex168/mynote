//
//  Date.swift
//  Notes
//
//  Created by Kheang on 5/11/21.
//

import Foundation

extension Date {
    func format() -> String {
        let formatter = DateFormatter()
            formatter.dateFormat = "EEEE h:mm"
        return formatter.string(from: self)
    }
}

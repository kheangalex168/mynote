//
//  ListNotesViewController.swift
//  Notes
//
//  Created by Kheang on 6/11/21.
//

import Foundation
import UIKit

protocol ListNotesDelegate: AnyObject {
    func refreshNotes()
    func deleteNote(with id: UUID)
}

class ListNotesViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    private let searchController = UISearchController()
    @IBOutlet weak var notesCountLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UINavigationItem!
    
    @IBOutlet weak var noteLabel: UILabel!
    
    private var allNotes: [Note] = [] {
        didSet {
            notesCountLabel.text = "\(allNotes.count)"
            
            noteLabel.text = "\(allNotes.count == 1 ? "Note" : "Notes")"
            filteredNotes = allNotes
        }
    }
    private var filteredNotes: [Note] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        tableView.contentInset = .init(top: 0, left: 0, bottom: 30, right: 0)
        configureSearchBar()
        
        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: "noteCustomCell")
        
        prepareUI()
    }
    
    private func indexForNote(id: UUID, in list: [Note]) -> IndexPath {
        let row = Int(list.firstIndex(where: { $0.id == id }) ?? 0)
        return IndexPath(row: row, section: 0)
    }
    
    private func configureSearchBar() {
        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.delegate = self
    }
    
    func prepareUI() {
        self.titleLabel.title = "My Notes".localizeSystem()
        self.noteLabel.text = "Note".localizeSystem()
        self.searchController.searchBar.placeholder = "Search".localizeSystem()
    }
    
    
    @IBAction func changeLanguagePressed(_ sender: Any) {
        
        let langAlert = UIAlertController(title: "Language", message: "Choose any Language", preferredStyle: .actionSheet)
        
        let khmerAction = UIAlertAction(title: "Khmer", style: .default){ _ in
            
            AppService.shared.changeLang(lang: "km-KH")
            self.title = "My Notes".localizedString()
            self.noteLabel.text = "Notes".localizedString()
            self.noteLabel.text = "Note".localizedString()
            self.searchController.searchBar.placeholder = "Search".localizedString()
        }
        
        let englishAction = UIAlertAction(title: "English", style: .default){ _ in
            
            AppService.shared.changeLang(lang: "en")
            self.title = "My Notes".localizedString()
            self.noteLabel.text = "Notes".localizedString()
            self.noteLabel.text = "Note".localizedString()
            self.searchController.searchBar.placeholder = "Search".localizedString()
        }
        
        let systemAction = UIAlertAction(title: "System", style: .default){ _ in
            
            self.titleLabel.title = "My Notes".localizeSystem()
            self.noteLabel.text = "Notes".localizeSystem()
            self.noteLabel.text = "Note".localizeSystem()
            self.searchController.searchBar.placeholder = "Search".localizeSystem()
        }
        
        langAlert.addAction(khmerAction)
        langAlert.addAction(englishAction)
        langAlert.addAction(systemAction)
        
        present(langAlert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func createNewNoteClicked(_ sender: UIButton) {
        goToEditNote(createNote())
    }
    
    private func goToEditNote(_ note: Note){
        let controller = storyboard?.instantiateViewController(identifier: EditNoteViewController.identifier) as! EditNoteViewController
        controller.note = note
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK:- Methods to implement
    private func createNote() -> Note {
        let note = Note()
        
        // Update table
        allNotes.insert(note, at: 0)
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        
        return note
    }
    
    private func fetchNotesFromStorage() {
        // TODO Get all saved notes
        print("Fetching all notes")
    }
    
    private func deleteNoteFromStorage(_ note: Note) {
        // TODO delete the note
        print("Deleting note")
        
        // Update the list
        deleteNote(with: note.id)
    }
    
    private func searchNotesFromStorage(_ text: String) {
        // TODO Get all notes that have this text
        print("Searching notes")
    }
}

// MARK: TableView Configuration
extension ListNotesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredNotes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCustomCell", for: indexPath) as! NoteTableViewCell
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: ListNoteTableViewCell.identifier) as! ListNoteTableViewCell
        
        cell.setup(note: filteredNotes[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        goToEditNote(filteredNotes[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteNoteFromStorage(filteredNotes[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

// MARK:- Search Controller Configuration
extension ListNotesViewController: UISearchControllerDelegate, UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search("")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, !query.isEmpty else { return }
        searchNotesFromStorage(query)
    }
    
    func search(_ query: String) {
        if query.count >= 1 {
            filteredNotes = allNotes.filter { $0.text.lowercased().contains(query.lowercased()) }
        } else{
            filteredNotes = allNotes
        }
        
        tableView.reloadData()
    }
}

// MARK:- ListNotes Delegate
extension ListNotesViewController: ListNotesDelegate {
    
    func refreshNotes() {
        allNotes = allNotes.sorted { $0.lastUpdate > $1.lastUpdate }
        tableView.reloadData()
    }
    
    func deleteNote(with id: UUID) {
        let indexPath = indexForNote(id: id, in: filteredNotes)
        filteredNotes.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        // just so that it doesn't come back when we search from the array
        allNotes.remove(at: indexForNote(id: id, in: allNotes).row)
    }
}


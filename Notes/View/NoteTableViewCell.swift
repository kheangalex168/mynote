import UIKit

class NoteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
func setup(note: Note) {
    titleLabel.text = note.title
    descLabel.text = note.desc
    dateLabel.text = note.date
}
    
}
